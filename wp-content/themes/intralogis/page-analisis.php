<?php get_header();?>
<section id="solution-item">
    <?php echo do_shortcode('[rev_slider alias="analisis"][/rev_slider]');?>
    <section id="info">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 offset-xl-2">
                    <div class="text">
                        <h2><span><strong>Diagnóstique,</strong> eva</span>lue, mejore.</h2>
                        <p>El diagnóstico logístico es un análisis de los diferentes procesos que tienen lugar dentro y fuera del almacén, en el que se evalúa el rendimiento, el estado y las particularidades de operativas como el almacenaje, el aprovisionamiento o la preparación de pedidos.
                            <a href="#">conozca nuestras soluciones en análisis.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="info2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 p-0">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/solution-item/item-1.jpg';?>')"></div>
                </div>
                <div class="col-xl-4 offset-xl-1">
                    <div class="text">
                        <h2><strong><span>Auditoria</span></strong> Logística.</h2>
                        <p>Recopilación y validación de los procesos actuales, una vez validado este avance, si es necesario, se llenarán los huecos con la elaboración de los procesos faltantes de tal manera que se tenga una visión completa de la operación actual (as is). </p>
                        <p>Elaboración de mejoras a los procesos con base a la Visión de la empresa.</p>
                        <a href="#">+ Más información</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="info2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1">
                    <div class="text">
                        <h2><strong>Diagramas</strong> de flujo.</h2>
                        <p>La mejor manera de identificar las oportunidades de mejora es la elaboración de Diagramas que muestren el flujo de las operaciones, sus participantes y el valor que agregan a cada proceso.</p>
                            <a href="#">+ Más información</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 p-0">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/solution-item/item-2.jpg';?>')"></div>
                </div>
            </div>
        </div>
    </section>
    <section class="info2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6 p-0">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/solution-item/item-3.jpg';?>')"></div>
                </div>
                <div class="col-xl-4 offset-xl-1">
                    <div class="text">
                        <h2><strong>Dimensionamiento</strong> de operaciones.</h2>
                        <p>Para lograr cumplir con el cliente en tiempo y forma se requiere contar con los recursos suficientes para la operación, cualquier excedente constituye un desperdicio y pérdidas de utilidad. Y quedar corto de recursos resultará en pérdida de competitividad y ventas, por lo que es muy importante determinar la cantidad de recursos requeridos: </p>
                        <ul>
                            <li>Recursos humanos, plantilla administrativa y operativa.</li>
                            <li>Equipos de manejo de materiales y de almacenaje.</li>
                            <li>Instalaciones: pisos, andenes, techos, altura.</li>
                            <li>Tecnologías de información: Hardware, Software y reportes.</li>
                            <li>Volúmenes de operación, indicadores de productividad.</li>
                        </ul>
                        <a href="#">+ Más información</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="info3">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <p>Conozca <a href="<?php echo home_url('casos-de-exito');?>">algunos casos de éxito</a> que hemos implementado en diversas industrias</p>
                </div>
            </div>
        </div>
    </section>
    <section id="slide">

        <div  class="splide" id="slide-big">
            <div class="splide__track">
                <ul class="splide__list">

                    <li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-3 offset-xl-1">

                                    <div class="text">
                                        <h4>Proyecto</h4>
                                        <h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
                                        <a href="#">ver</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-3 offset-xl-1">

                                    <div class="text">
                                        <h4>Proyecto</h4>
                                        <h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
                                        <a href="#">ver</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>

                    <li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-3 offset-xl-1">

                                    <div class="text">
                                        <h4>Proyecto</h4>
                                        <h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
                                        <a href="#">ver</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
        </div>

    </section>
    <section id="info5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><strong><span>Singergia experi</span>mentada</strong> entre conocimiento, hardware y software.</h2>
                        <p>Conozca más sobre nuestras <a href="#">soluciones</a>, <a href="#">expertise</a> y <a
                                    href="#">sobre nosotros</a></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-6 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-6 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="yellow">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/play.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section id="info4">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text">
                        <h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
                        <p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                    </div>
                </div>
                <div class="col-xl-5 p-relative">
                    <div id="form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">
</section>
<?php get_footer();?>
<script>
    new Splide( '#slide-big',{
        type: 'loop',
        perPage: 1.5,
        autoplay: true,
        focus: 'center',
    }).mount();
</script>
