<?php get_header();?>
<?php echo do_shortcode('[rev_slider alias="homepage"][/rev_slider]');?>

<section id="home">
	<section id="info">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-4 offset-xl-1 col-lg-12">
					<div class="text">
						<h2><span><strong>Knowhow,</strong> la ll</span>ave para ir al siguiente nivel.</h2>
						<p>Con casos de consultoría e implementación registrados en industrias de diferentes sectores y tamaños hemos recorrido múltiples cadenas logísticas que nos soportan con amplia experiencia en el ramo para afrontar exitosamente cualquier reto. Desde industrias de alimentos hasta automotriz, <a href="#">conozca algunos.</a></p>
					</div>
				</div>
				<div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

					<div class="row">
						<div class="col-lg-6 col-md-6 col-6 p-0">
							<img src="<?php echo get_stylesheet_directory_uri().'/img/home/info-1.jpg';?>" class="img-fluid">
						</div>
						<div class="col-lg-6 col-md-6 col-6 p-0">
							<img src="<?php echo get_stylesheet_directory_uri().'/img/home/info-2.jpg';?>" class="img-fluid">
						</div>
					</div>

				</div>
			</div>		
		</div>
	</section>
	<section id="slide">

		<div  class="splide" id="slide-big">
		  <div class="splide__track">
			<ul class="splide__list">

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

			</ul>
		  </div>
		</div>

	</section>
	<section id="info2">
		<img src="<?php echo get_stylesheet_directory_uri().'/img/home/logo.png';?>" class="img-fluid logo">
		<div class="container">
			<div class="row">
				<div class="col-xl-5 col-lg-4">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon.png';?>" class="img-fluid">
				</div>
				<div class="col-xl-5 col-lg-8">
					<div class="text">
						<h2><strong>Perspectiva global,</strong> soluciones integrales.</h2>
						<p>Nuestra oferta estratégica intralogística está cimentada en seis pilares que contemplan los ángulos más críticos de evaluación, diagnóstico y proyección que nos permita diseñar soluciones integrales para cualquier industria, aportando valor, durabilidad y rentabilidad en cada uno de ellos, estos son:</p>
					</div>
				</div>
			</div>
		</div>
		<ul class="circles">
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>');"></div>
				<h4>Tecnología</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>');"></div>
				<h4>Personas</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>');"></div>
				<h4>Procesos</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-4.png';?>');"></div>
				<h4>Seguridad</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-5.png';?>');"></div>
				<h4>Calidad</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-6.png';?>');"></div>
				<h4>Productividad</h4>
			</li>
		</ul>
		<div class="clear"></div>
	</section>
	<section id="yellow">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/play.png';?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="info3">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-4 offset-xl-2">
					<div class="text">
						<h2><span>Sinergia inteligente</span> entre conocimiento, hadware y software.</h2>
						<p>Nuestra experiencia, relaciones y alianzas comerciales nos permiten consolidar para nuestros clientes beneficios comerciales en la adquisición e implementación de equipos y servicios especiales para cada proyecto. <strong>Conozca nuestra oferta de productos.</strong></p>
					</div>
				</div>
				<div class="col-xl-5 offset-xl-1 p-0">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/bg-sinergia.jpg';?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section id="info4">
		<div class="container">
			<div class="row">
				<div class="col-xl-6">
					<div class="text">
						<h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
						<p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
					</div>
				</div>
				<div class="col-xl-5 p-relative">
					<div id="form">
						<?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">
</section>

<?php get_footer();?>
<script>
    new Splide( '#slide-big',{
        type: 'loop',
        perPage: 1.5,
        autoplay: true,
        focus: 'center',
		breakpoints: {
            768: {
                perPage: 1,
                focus:false,
            },
        }
    }).mount();
</script>

