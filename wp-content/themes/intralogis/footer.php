<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

            <footer>

               <div class="container">
                    <div class="row">
                       <div class="col-xl-4 offset-xl-4">
                           <img src="<?php echo get_stylesheet_directory_uri().'/img/home/logo.png';?>" class="img-fluid">
                       </div>
                    </div>
                   <div class="row">
                       <div class="col-xl-3 col-lg-6">
                           <h3>Análisis.</h3>
                           <ul>
                               <li><a href="#">Auditoria Logística.</a></li>
                               <li><a href="#">Diagramas de Flujo.</a></li>
                               <li><a href="#">Dimensionamiento de Operaciones.</a></li>
                           </ul>
                           <h3>Diseño de centros de distribución.</h3>
                           <ul>
                               <li><a href="#">Diseño de Red Logística.</a></li>
                               <li><a href="#">Definición de Modelos Operativos.</a></li>
                               <li><a href="#">Distribución de Planta (Layouting).</a></li>
                               <li><a href="#">Sistemas de Manejo de Información.</a></li>
                           </ul>
                           <h3>Construcción.</h3>
                           <ul>
                               <li><a href="#">Sistemas de Manejo de Materiales.</a></li>
                               <li><a href="#">Sistemas de Almacenaje.</a></li>
                               <li><a href="#">Equipos de transporte.</a></li>
                               <li><a href="#">Automatización de Almacenes.</a></li>
                           </ul>
                       </div>
                       <div class="col-xl-3 col-lg-6">
                           <h3>Implementación.</h3>
                           <ul>
                               <li><a href="#">Procedicimientos Estandár de Operación.</a></li>
                               <li><a href="#">Administración del Proyecto.</a></li>
                               <li><a href="#">Supervición de instalación.</a></li>
                           </ul>
                           <h3>Puesta en marcha.</h3>
                           <ul>
                               <li><a href="#">Capacitación y Entrenamiento.</a></li>
                               <li><a href="#">Balance Score Card.</a></li>
                               <li><a href="#">Indicadores de Desempeño.</a></li>
                           </ul>
                       </div>
                       <div class="col-xl-3 col-lg-6">
                            <h3>Materiales de empaque.</h3>
                            <ul>
                                <li><a href="#">Pelicula estirable (Playo).</a></li>
                                <li><a href="#">Relleno de empaque.</a></li>
                            </ul>
                            <h3>Manejo de materiales.</h3>
                            <ul>
                                <li><a href="#">Palets de Madera ISO II.</a></li>
                                <li><a href="#">Palets de Plástico.</a></li>
                                <li><a href="#">Palets de Exportación.</a></li>
                            </ul>
                            <h3>Equipos de surtido.</h3>
                            <ul>
                                <li><a href="#">Transpaletas hidráulicas.</a></li>
                                <li><a href="#">Carrros góndola</a></li>
                                <li><a href="#">Carros surtidores.</a></li>
                            </ul>
                       </div>
                       <div class="col-xl-3 col-lg-6">
                           <h3>Equipos de almacenaje.</h3>
                           <ul>
                               <li><a href="#">Racks Selectivos.</a></li>
                               <li><a href="#">Racks Dinámicos.</a></li>
                               <li><a href="#">Estantería para Cajas.</a></li>
                               <li><a href="#">Carton Flow.</a></li>
                           </ul>
                           <h3>Equipos automatizados.</h3>
                           <ul>
                               <li><a href="#">Transportadores de gravedad.</a></li>
                               <li><a href="#">Transportadores motorizados.</a></li>
                               <li><a href="#">Sistemas de Surtido por Luz (Pick to light).</a></li>
                           </ul>
                       </div>
                   </div>
               </div>            
            </footer>
            <img src="<?php echo get_stylesheet_directory_uri().'/img/home/footer-bottom.jpg';?>" class="img-fluid">

            <div class="top-button position-fixed zi-1020">
                <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
            </div>

        </div><!-- #page -->

        <?php wp_footer(); ?>

    </body>
</html>
