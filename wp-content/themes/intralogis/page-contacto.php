<?php get_header();?>
<?php echo do_shortcode('[rev_slider alias="contact"][/rev_slider]');?>
<section id="contact-page">

    <section id="info">
		<div class="container">
			<div class="row">
				<div class="col-xl-6">
					<div class="text">
						<h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
						<p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                        <p class="phones">Comúniquese al <a href="tel:+525555555555">+52 55 5555 5555</a> ó <a href="mailto:ventas@intralogis.mx">ventas@intralogis.mx</a></p>
					</div>
				</div>
				<div class="col-xl-5 offset-xl-1 p-relative">
					<div id="form">
						<?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
					</div>
				</div>
			</div>
		</div>
	</section>

    <section id="info2">
		<img src="<?php echo get_stylesheet_directory_uri().'/img/home/logo.png';?>" class="img-fluid logo">
		<div class="container">
			<div class="row">
				<div class="col-xl-5 col-lg-4">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon.png';?>" class="img-fluid">
				</div>
				<div class="col-xl-5 col-lg-8">
					<div class="text">
						<h2><strong>Perspectiva global,</strong> soluciones integrales.</h2>
						<p>Nuestra oferta estratégica intralogística está cimentada en seis pilares que contemplan los ángulos más críticos de evaluación, diagnóstico y proyección que nos permita diseñar soluciones integrales para cualquier industria, aportando valor, durabilidad y rentabilidad en cada uno de ellos, estos son:</p>
					</div>
				</div>
			</div>
		</div>
		<ul class="circles">
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>');"></div>
				<h4>Tecnología</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>');"></div>
				<h4>Personas</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>');"></div>
				<h4>Procesos</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-4.png';?>');"></div>
				<h4>Seguridad</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-5.png';?>');"></div>
				<h4>Calidad</h4>
			</li>
			<li>
				<div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-6.png';?>');"></div>
				<h4>Productividad</h4>
			</li>
		</ul>
		<div class="clear"></div>
	</section>

    <section id="yellow">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/play.png';?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>

    <section id="info3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><span><strong>Conozca nuestras</strong></span> soluciones y <a href="#">productos</a> estratégicos..</h2>
                        <p>La sinergia inteligente entre conocimiento, hadware y software. <strong>Juntos al siguiente nivel.</strong></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
                    <h3>Artículos que le pueden interesar de nuestro <a href="<php echo home_url('blog');?>">blog</a>.</h3>

                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>

                    <div id="slide-three" class="splide">

                        <div class="splide__track">

                            <ul class="splide__list">

                                <?php while($q->have_posts()): $q->the_post() ?>

                                    <li class="splide__slide">
                                        <a href="<?php the_permalink();?>">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                                    </div>
                                                    <h3><?php the_title();?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile ?>

                            </ul>

                        </div>
                    </div>

                    <h3>Conozca <a href="#">algunos casos de éxito</a> que hemos implementado en diversas industrias</h3>
                </div>
            </div>
        </div>
    </section>

    <section id="splide">

		<div class="splide" id="slide-big">
		  <div class="splide__track">
			<ul class="splide__list">

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

			</ul>
		  </div>
		</div>

	</section>

</section>

<?php get_footer();?>
<script>
    jQuery(document).ready(function(){
        new Splide( '#slide-three',{
            type: 'loop',
            perPage: 3,
            gap: '6em',
            pagination:false,
            autoplay: true,
            focus: 'center',
			breakpoints: {
				1366: {
					perPage: 3,
					gap:'2em'
				},
				992: {
					perPage: 3,
					gap:'1em'
				},
				768: {
					perPage: 2,
					focus:false,
					gap:'1em'
				},
				768: {
					perPage: 1,
					focus:false,
				},
			}
        }).mount();

        new Splide( '#slide-big',{
            type: 'loop',
            perPage: 1.5,
            autoplay: true,
            focus: 'center',
			breakpoints: {
				768: {
					perPage: 1,
					focus:false,
				},
			}
        }).mount();
    });
</script>
