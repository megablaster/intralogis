<?php get_header();?>
<section id="exito">
    <?php echo do_shortcode('[rev_slider alias="exito"][/rev_slider]');?>
    <section id="info">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-7">
                    <div class="text">
                        <h2><strong><span>Casos de éxito</span></strong> registrados en diversas industrias.</h2>
                        <p>Con casos de consultoría e implementación registrados en industrias de diferentes sectores y tamaños hemos recorrido múltiples cadenas logísticas que nos soportan con amplia experiencia en el ramo para afrontar exitosamente cualquier reto. Desde industrias de alimentos hasta automotriz, conozca algunos.</p>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-5">
                    <h3>Industrias</h3>
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/exito/industrial.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section id="filter">

        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="clearfix">
                        <div id="js-filters-blog-posts" class="cbp-l-filters-list cbp-l-filters-left">
                            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item cbp-l-filters-list-first">Todos (<div class="cbp-filter-counter"></div>)</div>
                            <div data-filter=".auto" class="cbp-filter-item">Automotriz (<div class="cbp-filter-counter"></div>)</div>
                            <div data-filter=".farma" class="cbp-filter-item">Farma (<div class="cbp-filter-counter"></div>)</div>
                            <div data-filter=".retail" class="cbp-filter-item">Retail (<div class="cbp-filter-counter"></div>)</div>
                            <div data-filter=".tech" class="cbp-filter-item cbp-l-filters-list-last">Tecnología (<div class="cbp-filter-counter"></div>)</div>
                        </div>
                    </div>

                    <div id="js-grid-blog-posts" class="cbp">

                        <?php
                            $args = array(
                                'post_type' => 'casos-de-exito',
                                'posts_per_page' => -1
                            );
                            $q = new WP_Query($args);
                        ?>

                        <?php while($q->have_posts()): $q->the_post() ?>

                            <div class="cbp-item auto">
                                <a href="<?php the_permalink();?>" class="cbp-caption">
                                    <div class="cbp-caption-defaultWrap">
                                        <img src="<?php the_field('imagen');?>" class="img-fluid">
                                    </div>
                                    <div class="cbp-caption-activeWrap">
                                        <div class="cbp-l-caption-alignCenter">
                                            <div class="cbp-l-caption-body" style="background-image:url('<?php the_field('imagen');?>');">
                                                <div class="cbp-l-caption-text">
                                                    <h3>Proyecto</h3>
                                                    <h2><span><?php the_title();?>:</span> <?php the_field('subtitulo');?></h2>
                                                    <h4 class="view">Ver</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                        <?php endwhile ?>

                    </div>
                   
                </div>
            </div>
        </div>

    </section>

    <section id="info2">
        <img src="<?php echo get_stylesheet_directory_uri().'/img/home/logo.png';?>" class="img-fluid logo">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon.png';?>" class="img-fluid icon">
                </div>
                <div class="col-xl-5 col-lg-8">
                    <div class="text">
                        <h2><strong>Perspectiva global,</strong> soluciones integrales.</h2>
                        <p>Nuestra oferta estratégica intralogística está cimentada en seis pilares que contemplan los ángulos más críticos de evaluación, diagnóstico y proyección que nos permita diseñar soluciones integrales para cualquier industria, aportando valor, durabilidad y rentabilidad en cada uno de ellos, estos son:</p>
                    </div>
                </div>
            </div>
        </div>
        <ul class="circles">
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>');"></div>
                <h4>Tecnología</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>');"></div>
                <h4>Personas</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>');"></div>
                <h4>Procesos</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-4.png';?>');"></div>
                <h4>Seguridad</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-5.png';?>');"></div>
                <h4>Calidad</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-6.png';?>');"></div>
                <h4>Productividad</h4>
            </li>
        </ul>
        <div class="clear"></div>
    </section>
    <section id="yellow">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/play.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section id="info3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><span><strong>Conozca nuestras</strong></span> soluciones y <a href="#">productos</a> estratégicos..</h2>
                        <p>La sinergia inteligente entre conocimiento, hadware y software. <strong>Juntos al siguiente nivel.</strong></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
                    <h3>Artículos que le pueden interesar de nuestro <a href="<php echo home_url('blog');?>">blog</a>.</h3>

                    <?php
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => -1
                    );

                    $q = new WP_Query($args);
                    ?>

                    <div id="slide-three" class="splide">

                        <div class="splide__track">

                            <ul class="splide__list">

                                <?php while($q->have_posts()): $q->the_post() ?>

                                    <li class="splide__slide">
                                        <a href="<?php the_permalink();?>">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                                    </div>
                                                    <h3><?php the_title();?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile ?>

                            </ul>

                        </div>
                    </div>

                    <h3>Conozca <a href="#">algunos casos de éxito</a> que hemos implementado en diversas industrias</h3>
                </div>
            </div>
        </div>
    </section>
    <section id="info4">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text">
                        <h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
                        <p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                    </div>
                </div>
                <div class="col-xl-5 p-relative">
                    <div id="form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">
</section>
<?php get_footer();?>
<script>
    jQuery(document).ready(function(){
        new Splide( '#slide-three',{
            type: 'loop',
            perPage: 3,
            gap: '6em',
            pagination:false,
            autoplay: true,
            focus: 'center',
            breakpoints: {
				1366: {
					perPage: 3,
					gap:'2em'
				},
				992: {
					perPage: 3,
					gap:'1em'
				},
				768: {
					perPage: 2,
					focus:false,
					gap:'1em'
				},
				768: {
					perPage: 1,
					focus:false,
				},
            }
        }).mount();
    });

    (function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio
    $('#js-grid-blog-posts').cubeportfolio({
        filters: '#js-filters-blog-posts',
        search: '#js-search-blog-posts',
        defaultFilter: '*',
        animationType: '3dflip',
        gapHorizontal: 70,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1500,
            cols: 2,
        }, {
            width: 1100,
            cols: 2,
        }, {
            width: 800,
            cols: 2,
        }, {
            width: 480,
            cols: 2,
            options: {
                caption: '',
                gapHorizontal: 50,
                gapVertical: 20,
            }
        }, {
            width: 320,
            cols: 1,
            options: {
                caption: '',
                gapHorizontal: 50,
            }
        }],
        caption: 'revealBottom',
        displayType: 'fadeIn',
        displayTypeSpeed: 400,
        });
    })(jQuery, window, document);
</script>
