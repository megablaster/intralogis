<?php get_header();?>
<div id="casos-item">

    <section id="header">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/cover.png';?>" class="img-fluid">
                </div>
                <div class="col-xl-5 offset-xl-1 col-lg-6 offset-lg-1">
                    <div class="text">
                        <a href="<?php echo home_url('casos-de-exito');?>" class="return">Casos de éxito</a>
                        <h1>Diseño de Cedis</h1>
                        <h2>Compañia de ventas por catálogo.</h2>
                        <h4>México: Toluca de lerdo | Retail</h4>
                        <p>Diseño de centro de distribución, calculo de personal y reingeniería de procesos de surtido para zonas de Ventas en cobertura Nacional.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="info-horizontal">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="cube">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/top-info.jpg';?>" class="img-fluid">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="text border-right">
                                    <div class="number">25,000</div>
                                    <h5>M<sup>2</sup> superficie</h5>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="text border-right">
                                    <div class="number">125,000</div>
                                    <h5>Piezas / día</h5>
                                </div>
                            </div>
                            <div class="col-lg-4 with-border">
                                <div class="text">
                                    <div class="number">+1,500</div>
                                    <h5>Sku´s</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="info-left">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 offset-lg-1 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-xs-2 order-2">
                    <div class="text">
                        <h2><span>Reto:</span> 100,000 clientes mensuales.</h2>
                        <p>Diseñar un Centro Operativo y de Distribución con la capacidad mensual bajo un proceso normal operativo sin poner en riesgo el ervicio a todos los clientes de la fuerza de ventas.</p>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-1 order-sm-1 order-xs-1 order-1">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/info-1.jpg';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="info-right">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1 col-lg-5 offset-lg-1">
                <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/info-2.jpg';?>" class="img-fluid">
                </div>
                <div class="col-xl-4 offset-xl-0 col-lg-4 offset-lg-1">
                    <div class="text">
                        <h2><span>Solución:</span> Llave en mano.</h2>
                        <p>Iniciamos el proceso con la búsqueda de una ubicación estratégica desde donde se pudieran atender a las más de 250 zonas de venta en el territorio nacional, identificando y eligiendo la mejor ubicación costo / beneficio dentro de un parque industrial en la zona de Toluca de Lerdo, espacio donde se implemento una solución llave en mano considerando las principales variables de negocio del cliente.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="info-left">
        <div class="container">
            <div class="row">
            <div class="col-lg-5 offset-lg-1 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-xs-2 order-2">
                    <div class="text">
                        <h2><span>Puesta en marcha:</span> 5 meses record.</h2>
                        <p>En un tiempo récord de 5 meses diseñamos, acondicionamos e implementamos los conocimientos, tecnologías y procesos necesarios en el centro de distribución y el centro de operaciones. Colateralmente se equiparon los sistemas de almacenamiento y súrtido de órdenes cumpliendo en paralelo con los estántares de nivel de servicio en tiempo y forma requeridos por todos los cleintes a nivel nacional.</p>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-1 order-sm-1 order-xs-1 order-1">
                     <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/info-3.jpg';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section id="cube">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="cube">
                        <div class="row">
                            <div class="col-xl-5 col-lg-4 col-md-4">
                                <div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/casos/result.jpg';?>');"></div>
                            </div>
                            <div class="col-xl-7 col-lg-7 col-md-8">
                                <div class="row">
                                    <div class="col-xl-8 offset-xl-2 col-lg-10 offset-lg-1 col-md-10 offset-md-1 p-rela">

                                        <div class="text">
                                            <h2><span>Resultados</span> de negocio.</h2>
                                            <ul>
                                                <li>Cumpliendo de los resultados financieros de acuerdo al presupuesto de inversión.</li>
                                                <li>5 años sin necesidad de aumento de capacidades.</li>
                                                <li>Implementación de las últimas tecnologías de manejo de materiales.</li>
                                                <li>125,000 piezas surtidas mensualmente.</li>
                                                <li>Capacidad de atención de más de 100,000 clientes mensuales.</li>
                                            </ul>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="simple-text">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <p>Conozca algunos <a href="#">casos de éxito</a> que hemos implementado en diversas industrias.</a></p>
                </div>
            </div>
        </div>
    </section>

    <section id="slide">

		<div  class="splide" id="slide-big">
		  <div class="splide__track">
			<ul class="splide__list">

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

				<li class="splide__slide" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/slide.jpg';?>');">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xl-3 offset-xl-1">

								<div class="text">
									<h4>Proyecto</h4>
									<h1><span>Palletles Nestle<sup>&reg;</sup></span> sistema de manejo unitizado de cargas.</h1>
									<a href="#">ver</a>
								</div>

							</div>
						</div>
					</div>
				</li>

			</ul>
		  </div>
		</div>

	</section>

    <section id="info4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><span><strong>Conozca nuestras</strong></span> soluciones y <a href="#">productos</a> estratégicos..</h2>
                        <p>La sinergia inteligente entre conocimiento, hadware y software. <strong>Juntos al siguiente nivel.</strong></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <h3>Artículos que le pueden interesar de nuestro <a href="<php echo home_url('blog');?>">blog</a>.</h3>

                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>

                    <div id="slide-three" class="splide">

                        <div class="splide__track">

                            <ul class="splide__list">

                                <?php while($q->have_posts()): $q->the_post() ?>

                                    <li class="splide__slide">
                                        <a href="<?php the_permalink();?>">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                                    </div>
                                                    <h3><?php the_title();?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile ?>

                            </ul>

                        </div>
                    </div>

                    <h3>Conozca <a href="#">algunos casos de éxito</a> que hemos implementado en diversas industrias</h3>

                </div>
            </div>
        </div>
    </section>
    <section id="info5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text">
                        <h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
                        <p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                    </div>
                </div>
                <div class="col-xl-5 p-relative">
                    <div id="form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">

</div>
<?php get_footer();?>

<script>
    new Splide( '#slide-big',{
            type: 'loop',
            perPage: 1.5,
            autoplay: true,
            focus: 'center',
			breakpoints: {
				768: {
					perPage: 1,
					focus:false,
				},
			}
    }).mount();

    new Splide( '#slide-three',{
        type: 'loop',
        perPage: 3,
        gap: '6em',
        pagination: false,
        autoplay: true,
        focus: 'center',
        breakpoints: {
            1366: {
                perPage: 3,
                gap:'2em'
            },
            992: {
                perPage: 3,
                gap:'1em'
            },
            768: {
                perPage: 2,
                focus:false,
                gap:'1em'
            },
            768: {
                perPage: 1,
                focus:false,
            },
        }
    }).mount();
</script>