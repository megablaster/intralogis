<?php get_header();?>
<?php echo do_shortcode('[rev_slider alias="about"][/rev_slider]');?>

<section id="about-page">
    <section id="info">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-8">
                    <div class="text">
                        <h2><strong><span>La sinergia ent</span>re conocimiento,</strong> experiencia y certidumbre.</h2>
                        <p>Intralogis® es la combinación de tres de los expertos con más conocimiento teórico y práctico en la industria intralogística de México con cientos de proyectos consumados exitosamente en industrias de todos los tamaños, y que a través de sus
                            <a href="#">vectores de desarollo</a> conciben, implementan y logran proyectos que generan  alta rentabilidad para cada uno de sus clientes.</p>
                    </div>
                </div>
                <div class="col-xl-4 offset-xl-2 col-lg-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/about/logo-black.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section id="yellow">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 text-center">
                    <h2><strong>Nuestra misión</strong> es Identificar oportunidades de mejora y convertirlas en propuestas tecnológicas rentables.</h2>
                    <p>Conozca más de nosotros en nuestro <a href="#">video resumen</a></p>
                </div>
            </div>
        </div>
    </section>
    <section id="info2">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-5">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/about/cover.jpg';?>" class="img-fluid">
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-6 offset-lg-1">
                    <div class="text">
                        <h2><strong>Más valor por su inversión.</strong> Resultados tangibles, medibles y valiosos.</h2>
                        <p>Ser reconocidos por nuestros clientes y competidores por la eficacia de nuestras soluciones es la principal visión de negocio que tenemos. Conozca algunos de nuestros
                            <a href="#">casos de éxito.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="info3">
        <img src="<?php echo get_stylesheet_directory_uri().'/img/home/logo.png';?>" class="img-fluid logo">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-8">
                    <div class="text">
                        <h2><strong>Nuestros vectores</strong> de desarrollo.</h2>
                        <p>Nuestra oferta estratégica intralogística está cimentada en seis pilares que contemplan los ángulos más críticos de evaluación, diagnóstico y proyección que nos permita diseñar soluciones integrales para cualquier industria, aportando valor, durabilidad y rentabilidad en cada uno de ellos, estos son:</p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-1 col-lg-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/icon.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
        <ul class="circles">
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>');"></div>
                <h4>Tecnología</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>');"></div>
                <h4>Personas</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>');"></div>
                <h4>Procesos</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-4.png';?>');"></div>
                <h4>Seguridad</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-5.png';?>');"></div>
                <h4>Calidad</h4>
            </li>
            <li>
                <div class="img-circle" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-6.png';?>');"></div>
                <h4>Productividad</h4>
            </li>
        </ul>
        <div class="clear"></div>
    </section>
    <section id="yellow-two">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/play.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section id="info4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><span><strong>Conozca nuestras</strong></span> soluciones y <a href="#">productos</a> estratégicos..</h2>
                        <p>La sinergia inteligente entre conocimiento, hadware y software. <strong>Juntos al siguiente nivel.</strong></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <h3>Artículos que le pueden interesar de nuestro <a href="<php echo home_url('blog');?>">blog</a>.</h3>

                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>

                    <div id="slide-three" class="splide">

                        <div class="splide__track">

                            <ul class="splide__list">

                                <?php while($q->have_posts()): $q->the_post() ?>

                                    <li class="splide__slide">
                                        <a href="<?php the_permalink();?>">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                                    </div>
                                                    <h3><?php the_title();?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile ?>

                            </ul>

                        </div>
                    </div>

                    <h3>Conozca <a href="#">algunos casos de éxito</a> que hemos implementado en diversas industrias</h3>

                </div>
            </div>
        </div>
    </section>
    <section id="info5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text">
                        <h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
                        <p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                    </div>
                </div>
                <div class="col-xl-5 p-relative">
                    <div id="form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">
</section>

<?php get_footer();?>
<script>
    new Splide( '#slide-three',{
        type: 'loop',
        perPage: 3,
        gap: '6em',
        pagination: false,
        autoplay: true,
        focus: 'center',
        breakpoints: {
            1366: {
                perPage: 3,
                gap:'2em'
            },
            992: {
                perPage: 3,
                gap:'1em'
            },
            768: {
                perPage: 2,
                focus:false,
                gap:'1em'
            },
            768: {
                perPage: 1,
                focus:false,
            },
        }
    }).mount();
</script>
