<?php get_header();?>
<div id="casos-item">

    <section id="header">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-4">
                    <div class="img-circle" style="background-image:url('<?php the_post_thumbnail_url();?>');"></div>
                </div>
                <div class="col-xl-5 offset-xl-1 col-lg-6 offset-lg-1">
                    <div class="text">
                        <a href="<?php echo home_url('page-casos-de-exito');?>" class="return">Casos de éxito</a>
                        <h1><?php the_title();?></h1>
                        <h2><?php the_field('subtitulo');?></h2>
                        <h4><?php the_field('lugar');?></h4>
                        <?php the_content();?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="info-horizontal">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <div class="cube">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/casos/top-info.jpg';?>" class="img-fluid">
                        <div class="row">
                            <div class="col-lg-4 p-relative">
                                <div class="text border-right">
                                    <div class="number"><?php the_field('superficie');?></div>
                                    <h5>M<sup>2</sup> superficie</h5>
                                </div>
                            </div>
                            <div class="col-lg-4 p-relative">
                                <div class="text border-right">
                                    <div class="number"><?php the_field('movimientos');?></div>
                                    <h5><?php the_field('movimiento_piezas_titulo');?></h5>
                                </div>
                            </div>
                            <div class="col-lg-4 with-border p-relative">
                                <div class="text">
                                    <div class="number"><?php the_field('sku');?></div>
                                    <h5>Sku´s</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="info-left">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 offset-lg-1 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-xs-2 order-2">
                    <div class="text">
                        <h2><span>Reto:</span> <?php the_field('reto_titulo');?></h2>
                        <p><?php the_field('reto_descripcion');?></p>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-1 order-sm-1 order-xs-1 order-1">
                    <img src="<?php the_field('reto_imagen');?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <section class="info-right">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-1 col-lg-5 offset-lg-1">
                <img src="<?php the_field('solucion_imagen');?>" class="img-fluid">
                </div>
                <div class="col-xl-4 offset-xl-0 col-lg-4 offset-lg-1">
                    <div class="text">
                        <h2><span>Solución:</span> <?php the_field('solucion_titulo');?></h2>
                        <p><?php the_field('solucion_descripcion');?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="info-left">
        <div class="container">
            <div class="row">
            <div class="col-lg-5 offset-lg-1 order-xl-1 order-lg-1 order-md-2 order-sm-2 order-xs-2 order-2">
                    <div class="text">
                        <h2><span>Puesta en marcha:</span> <?php the_field('puesta_titulo');?></h2>
                        <p><?php the_field('solucion_descripcion');?></p>
                    </div>
                </div>
                <div class="col-lg-4 offset-lg-1 order-sm-1 order-xs-1 order-1">
                     <img src="<?php the_field('puesta_imagen');?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>

    <?php if(get_field('resultados_descripcion')):?>
        <section id="cube">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="cube">
                            <div class="row">
                                <div class="col-xl-5 col-lg-4 col-md-4">
                                    <div class="img"
                                    style="background-image:url('<?php the_field('resultados_imagen');?>');"></div>
                                </div>
                                <div class="col-xl-7 col-lg-7 col-md-8">
                                    <div class="row">
                                        <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-10 offset-md-1 p-rela">

                                            <div class="text">
                                                <h2><span>Resultados</span> de negocio.</h2>
                                                <?php the_field('resultados_descripcion');?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif ?>

    <section class="simple-text">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 text-center">
                    <p>Conozca algunos <a href="<?php echo home_url('page-casos-de-exito');?>">casos de éxito</a> que hemos implementado en diversas industrias.</a></p>
                </div>
            </div>
        </div>
    </section>

    <section id="slide">

		<div  class="splide" id="slide-big">
		  <div class="splide__track">
			<ul class="splide__list">

                <?php
                    $args = array(
                        'post_type' => 'casos-de-exito',
                        'posts_per_page' => -1
                    );
                    $q = new WP_Query($args);
                ?>

                <?php while($q->have_posts()): $q->the_post() ?>

                    <li class="splide__slide" style="background-image:url('<?php the_field('imagen');?>');">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xl-3 offset-xl-1">

                                    <div class="text">
                                        <h4>Proyecto</h4>
                                        <h1><span><?php the_title();?><sup>&reg;</sup></span> <?php the_field('subtitulo');?></h1>
                                        <a href="<?php the_permalink();?>">ver</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </li>

                <?php endwhile ?>

                <?php wp_reset_postdata();?>

			</ul>
		  </div>
		</div>

	</section>

    <section id="info4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-12">
                    <div class="text">
                        <h2><span><strong>Conozca nuestras</strong></span> soluciones y <a href="#">productos</a> estratégicos..</h2>
                        <p>La sinergia inteligente entre conocimiento, hadware y software. <strong>Juntos al siguiente nivel.</strong></p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-12 p-0">

                    <div class="row">
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img img-1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-1.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Diseño de Cedis</h3>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-12 p-0">
                            <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/contact/soluciones-2.jpg'; ?>');">
                                <div class="top">
                                    Auditoría logística<br>
                                    Diagramas de flujo<br>
                                    Dimensionamiento de operaciones
                                </div>
                                <div class="center">
                                    <a href="#">
                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                    </a>
                                </div>
                                <div class="bottom">
                                    <h4>Soluciones</h4>
                                    <h3>Análisis</h3>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section id="blog">
        <div class="container">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">

                    <h3>Artículos que le pueden interesar de nuestro <a href="<php echo home_url('blog');?>">blog</a>.</h3>

                    <?php
                        $args = array(
                            'post_type' => 'post',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>

                    <div id="slide-three" class="splide">

                        <div class="splide__track">

                            <ul class="splide__list">

                                <?php while($q->have_posts()): $q->the_post() ?>

                                    <li class="splide__slide">
                                        <a href="<?php the_permalink();?>">
                                            <div class="row">
                                                <div class="col-xl-12">
                                                    <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');">
                                                        <i class="fas fa-long-arrow-alt-right fa-3x"></i>
                                                    </div>
                                                    <h3><?php the_title();?></h3>
                                                </div>
                                            </div>
                                        </a>
                                    </li>

                                <?php endwhile ?>

                            </ul>

                        </div>
                    </div>

                    <h3>Conozca <a href="#">algunos casos de éxito</a> que hemos implementado en diversas industrias</h3>

                </div>
            </div>
        </div>
    </section>
    <section id="info5">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="text">
                        <h2><strong>El conocimiento es la llave,</strong> la experiencia el camino.</h2>
                        <p>Agende una llamada con un experto de intralogis® hoy mismo y lleve su operación al siguiente nivel logístico.</p>
                    </div>
                </div>
                <div class="col-xl-5 p-relative">
                    <div id="form">
                        <?php echo do_shortcode('[contact-form-7 id="26" title="Contact home"]');?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/llave-bg.jpg';?>" class="img-fluid">

</div>
<?php get_footer();?>

<script>
    new Splide( '#slide-big',{
            type: 'loop',
            perPage: 1.5,
            autoplay: true,
            focus: 'center',
			breakpoints: {
				768: {
					perPage: 1,
					focus:false,
				},
			}
    }).mount();

    new Splide( '#slide-three',{
        type: 'loop',
        perPage: 3,
        gap: '6em',
        pagination: false,
        autoplay: true,
        focus: 'center',
        breakpoints: {
            1366: {
                perPage: 3,
                gap:'2em'
            },
            992: {
                perPage: 3,
                gap:'1em'
            },
            768: {
                perPage: 2,
                focus:false,
                gap:'1em'
            },
            768: {
                perPage: 1,
                focus:false,
            },
        }
    }).mount();
</script>