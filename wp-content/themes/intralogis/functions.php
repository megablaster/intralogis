<?php

// style and scripts
add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
function bootscore_5_child_enqueue_styles() {
   
   // style.css
   if(is_page('page-casos-de-exito')){
      wp_enqueue_style( 'cube-css', get_stylesheet_directory_uri() . '/script/cubeportfolio/css/cubeportfolio.min.css' );
      wp_enqueue_script('cube-js', get_stylesheet_directory_uri() . '/script/cubeportfolio/js/jquery.cubeportfolio.min.js', false, '', true);
   }

   wp_enqueue_style( 'splide-css', get_stylesheet_directory_uri() . '/script/splide/css/splide.min.css' );
   wp_enqueue_style( 'splide-theme-css', get_stylesheet_directory_uri() . '/script/splide/css/themes/splide-default.min.css' );
   wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
   
   // custom.js
   wp_enqueue_script('splide-js', get_stylesheet_directory_uri() . '/script/splide/js/splide.min.js', false, '', true);
   wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);

} 

add_filter('wpcf7_autop_or_not', '__return_false');
